import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs'
import { Storage } from '@ionic/storage'
@Injectable()
export class ItemServiceProvider {
  data: any;
  items: any;
  url = 'http://food-app-thenightsaredarkandfullofterrors.c9users.io:8080';
  constructor(public http: Http, public storage : Storage) {

  }

  public getItems = (page_number): Observable<any> => {
    return this.http.get(`${this.url}/items?page=${page_number}`).map(data => data.json());
  }
  public getMoreItems = (page_number, term): Observable<any> => {
    return this.http.get(`${this.url}/search/${term}?page=${page_number}`).map(data => data.json());
  }

  public getItemRecipesById = (id): Observable<any> => {
    return this.http.get(`${this.url}/items/${id}/recipes`).map(data => data.text());
  }

  public getInstructions = (Item_id, recipe_id): Observable<any> => {
    return this.http.get(`${this.url}/items/${Item_id}/recipes/${recipe_id}/instructions`).map(data => data.text());
  }
  public getIngredients = (Item_id, recipe_id): Observable<any> => {
    return this.http.get(`${this.url}/items/${Item_id}/recipes/${recipe_id}/ingredients`).map(data => data.json());
  }
  public getItemImages = (item_id): Observable<any> => {
    return this.http.get(`${this.url}/items/${item_id}/item_images`).map(data => data.text());
  }
 
  public getSpecialItems = (): Observable<any> => {
    return this.http.get(`${this.url}/items/special_items`).map(data => data.json());
  }

  public getItemsFromStorage = () : Promise<any> => {
    return this.storage.get('items')
  }
  public getAndStoreAllItems = () => {
    this.http.get(`${this.url}/all_items`).subscribe(items => {
      console.log("Items being stored in storage: ", items.json())
      this.storage.set('items', JSON.stringify(items.json()))
    })
  }
}


