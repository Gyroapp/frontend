import { Component, ViewChild } from "@angular/core";
import {
  AlertController,
  IonicPage,
  NavController,
  Nav,
  Slides,
  Platform,
  ToastController
} from "ionic-angular";
import { ItemServiceProvider } from "../../providers/item-service/item-service";
import { TabsPage } from "../tabs/tabs";
import { DataServiceProvider } from "../../providers/data-service/data-service";
import * as groupArray from "group-array";
@IonicPage()
@Component({
  selector: "page-items",
  templateUrl: "items.html",
  providers: [ItemServiceProvider, Nav]
})
export class ItemsPage {
  items: any;
  recipes: any;
  searchTerm: string;
  filteredItems = [];
  currFilterValues: Array<any> = [];
  cuisines: Array<string> = [
    "Indian",
    "Italian",
    "Chinese",
    "French",
    "Mexican"
  ];
  page_number: number = 1;
  search_page: number = 1;
  search: boolean = false;
  reach_end: boolean = false;
  rate = 2.5;
  cart_items: Array<any> = [];
  user_id: number;
  navBarColor = "light";
  unchangedFilteredItems: Array<any> = [];
  counter = 11;
  local = false;
  allItems = [];
  @ViewChild(Slides) slides: Slides;
  constructor(
    public dataService: DataServiceProvider,
    private toastCtrl: ToastController,
    public platform: Platform,
    private alertCtrl: AlertController,
    public nav: NavController,
    public itemService: ItemServiceProvider
  ) {
    this.userAndCartAssign();
    this.itemsAssign();
  }
  itemsAssign() {
   // this.itemService.getItemsFromStorage().then(items => {
     // if (!items) {
        this.itemService.getItems(this.page_number).subscribe(data => {
          this.items = data;
          this.filteredItems = this.items;
          this.unchangedFilteredItems = this.filteredItems;
        //  this.itemService.getAndStoreAllItems();
          //this.assignImagesArray();
       // });
     /* } else {
        this.allItems = JSON.parse(items);
        console.log("Items found in storage: ", this.items);
        this.items = this.allItems.slice(0, this.counter);
        this.filteredItems = this.items;
        this.unchangedFilteredItems = this.filteredItems;
        this.local = true;
      } */
    })
  }

  userAndCartAssign() {
    this.dataService.getData().then(data => {
      if (data != null) {
        this.user_id = JSON.parse(data).id;
      }
    });
  }
  assignImagesArray() {
    this.items.forEach(item => {
      if (item.imageUrls == null) {
        this.itemService.getItemImages(item.id).subscribe(data => {
          item.imageUrls = JSON.parse(data);
        });
      }
    });
  }
  assignImagesSearchItemsArray() {
    this.filteredItems.forEach(item => {
      if (item.imageUrls == null) {
        this.itemService.getItemImages(item.id).subscribe(data => {
          item.imageUrls = JSON.parse(data);
        });
      }
    });
  }

  itemClick(item) {
    this.nav.push(TabsPage, { data: item['id'] });
  }

  setFilteredItems() {
    if (this.searchTerm.length == 0) {
      this.page_number = 1;
      this.search_page = 1;
      this.search = false;
    }
    this.filteredItems = this.filterItems() || [];
    if (this.filteredItems.length == 0) {
      this.itemService
        .getMoreItems(this.search_page, this.searchTerm)
        .subscribe(data => {
          this.filteredItems = data;
          this.search = true;
          //  this.assignImagesSearchItemsArray()
          console.log("filtered items", this.filteredItems);
        });
    }
  }
  setFilteredItemsByFilters() {
    //console.log(this.currFilterValues);
    this.filterItemsByFilters();
  }
  filterItemsByFilters() {
    console.log("acasdsa", this.currFilterValues);
    if (this.currFilterValues.length == 0) {
      this.filteredItems = this.items;
      this.unchangedFilteredItems = this.filteredItems;
    } else {
      let groupedCurrFilterValues = groupArray(this.currFilterValues, "type");
      this.filteredItems = [];
      for (let key in groupedCurrFilterValues) {
        let tempkey = key;
        groupedCurrFilterValues[key].forEach(element => {
          if (key == tempkey) {
            this.filteredItems = this.filteredItems.concat(
              this.items.filter(item => {
                return (
                  item[key].toLowerCase() == element["value"].toLowerCase()
                );
              })
            );
            console.log(this.filteredItems);
          } else {
            this.filteredItems = this.filteredItems.filter(item => {
              return item[key].toLowerCase() == element["value"].toLowerCase();
            });
          }
        });
        //this.currFilterValues.forEach(filter => {});
      }
      this.unchangedFilteredItems = this.filteredItems;
    }
  }

  filterItems() {
    //console.log("filtered items unchanged: ", this.unchangedFilteredItems);
    this.filteredItems = this.filteredItems || [];
    if (this.filteredItems.length == 0) this.filteredItems = this.items;
    return this.unchangedFilteredItems.filter(item => {
      return (
        item["name"].toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1
      );
    });
  }
  onCancel() {
    this.search_page = 1;
    this.search = false;
    console.log("Cancel event fired");
    this.setFilteredItems();
  }
  showSetFiltersAlert() {
    let alert = this.alertCtrl.create({
      title: "Set filters",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel");
          }
        },
        {
          text: "Set selected",
          handler: () => {
            //   console.log('Set clicked');
            this.setFilteredItemsByFilters();
          }
        }
      ],
      inputs: [
        {
          type: "checkbox",
          label: "Cuisine",
          checked: this.filterCheckedCondition("cuisine"),
          handler: currInput => {
            currInput.checked = this.filterCheckedCondition(
              currInput.label.toLowerCase()
            );

            this.showFilter("cuisine", currInput);
          }
        }
      ]
    });
    alert.present();
  }

  filterCheckedCondition(label) {
    for (let i = 0; i < this.currFilterValues.length; i++) {
      if (this.currFilterValues[i].type == label) {
        //  console.log("yOu aRe tHe lOvE cHaRgEr")
        return true;
      }
    }
    return false;
  }

  filterOptionCheckedCondition(label) {
    for (let i = 0; i < this.currFilterValues.length; i++) {
      if (this.currFilterValues[i].value == label) {
        //console.log("yOu aRe tHe lOvE cHaRgEr")
        return true;
      }
    }
    return false;
  }

  showFilter(type: string, currInput) {
    //console.log("inside show filter");
    let inputArray = [];
    this[`${type}s`].forEach(current => {
      inputArray.push({
        type: "checkbox",
        label: current,
        checked: this.filterOptionCheckedCondition(current),
        name: `${type}`,
        handler: data => {
          if (data.checked) {
            let currFilter = { type: type, value: data.label };
            this.currFilterValues.push(currFilter);
            //    console.log(this.currFilterValues);
          } else {
            let index = this.currFilterValues.indexOf(data.label);
            this.currFilterValues.splice(index, 1);
          }
          if (this.currFilterValues.length > 0) currInput.checked = true;
        }
      });
    });
    let alert = this.alertCtrl.create({
      inputs: inputArray,
      buttons: [
        {
          text: `I've selected my favorite ${type}s`,
          handler: () => {
            //    console.log(this.currFilterValues);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.currFilterValues = [];
            //           console.log(this.currFilterValues);
          }
        }
      ]
    });
    alert.present();
    //console.log(inputArray);
    // console.log(this.currFilterValues);
  }

  doInfinite(): Promise<any> {
      let tmp = [];
      return new Promise(resolve => {
        setTimeout(() => {
          if (!this.search) {
            this.itemService.getItems(++this.page_number).subscribe(data => {
              tmp = data;
              if (tmp.length == 0) {
                this.presentToast();
              } else {
                tmp.forEach(element => {
                  this.items.push(element);
                });
                this.filteredItems = this.items;
                //    this.assignImagesArray();
              }
            });
          } else {
            this.itemService
              .getMoreItems(++this.search_page, this.searchTerm)
              .subscribe(data => {
                tmp = data;
                if (tmp.length == 0) {
                  this.presentToast();
                } else {
                  tmp.forEach(element => {
                    this.items.push(element);
                    this.filteredItems.push(element);
                  });
                  //  this.assignImagesArray();
                }
              });
          }
          resolve();
        }, 1000);
      });
    
  }
  presentToast() {
    let toast = this.toastCtrl.create({
      message: "Woah! looks like you reached the end",
      duration: 3000
    });
    toast.present();
  }
  addToCart(item) {
    this.cart_items.push(item);
    this.dataService.addToCart(item, this.user_id).subscribe(
      res => {
        if (res.status == 200) {
          console.log("Yay! Added to cart");
          let toast = this.toastCtrl.create({
            message: "Item successfully added to you cart",
            duration: 3000,
            position: "middle"
          });
          toast.present();
        } else {
          console.log("Already in your cart");
          let toast = this.toastCtrl.create({
            message: "Item is already in your cart",
            duration: 3000,
            position: "middle"
          });
          toast.present();
        }
      },
      err => {
        console.log("Error in here");
        let toast = this.toastCtrl.create({
          message: "Item cannot be added in your cart",
          duration: 3000,
          position: "middle"
        });
        toast.present();
      }
    );
  }
}
