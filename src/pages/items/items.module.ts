import { NgModule } from '@angular/core';
import { Nav, IonicPageModule } from 'ionic-angular';
import { ItemsPage } from './items';

@NgModule({
  declarations: [
    ItemsPage,
  ],
  imports: [
    IonicPageModule.forChild(ItemsPage),
  ],
  exports: [
    ItemsPage
  ],
  providers: [Nav]
})
export class ItemsPageModule {}
