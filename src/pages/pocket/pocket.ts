import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service'
import { TabsPage } from "../tabs/tabs";

/**
 * Generated class for the PocketPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-pocket',
  templateUrl: 'pocket.html',
})
export class PocketPage {
  pocket = {}
  constructor(public navCtrl: NavController, public navParams: NavParams, public dataService : DataServiceProvider) {
    this.assignPocket()
  }
  assignPocket() {
    this.dataService.getPocketFromStorage().then(pocket => {
      this.pocket = JSON.parse(pocket)
    })
  
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PocketPage');
  }
  removeFromPocket(recipe_id) {
    this.dataService.removeFromPocket(recipe_id, this.pocket['user_id']).subscribe(pocket => {
      this.pocket = pocket
      this.dataService.updateLocalPocket(pocket)
    })
  }
  viewRecipe(recipe){
    this.navCtrl.push(TabsPage, {data: recipe["item_id"]})
  }
}
