import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoppingBagPage } from './shopping-bag';

@NgModule({
  declarations: [
    ShoppingBagPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoppingBagPage),
  ],
  exports: [
    ShoppingBagPage
  ]
})
export class ShoppingBagPageModule {}
