import { Component, OnInit } from '@angular/core'
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { DataServiceProvider } from '../../providers/data-service/data-service'
@IonicPage()
@Component({
  selector: 'page-shopping-bag',
  templateUrl: 'shopping-bag.html',
})
export class ShoppingBagPage implements OnInit {
  shopping_items
  user_id: number
  statuses
  existenceCheck = []
  strikes = {}
  constructor(public navCtrl: NavController, public navParams: NavParams, private dataService: DataServiceProvider) {
  }
  ngOnInit() {
    this.dataService.getData().then(data => {
      this.user_id = JSON.parse(data).id;
      this.dataService.getShoppingBag(this.user_id).then(data => {
        this.shopping_items = data[0]
        this.statuses = data[1]
        this.existenceCheck = data[2]
        this.dataService.getStrikes().then(strikes => {
          this.strikes = JSON.parse(strikes) || {}
        })
      })
    })
  }
  existenceCheckFunc(id) {
    return this.existenceCheck.includes(id)
  }
  editShoppingBag(item, type) {
    if (type == "del") {
      this.statuses[`${item["recipe_id"]}`][item["id"]] = false
      this.existenceCheck.splice(this.existenceCheck.indexOf(item["id"]), 1)
    }
    else if (type == "add") {
      this.statuses[`${item["recipe_id"]}`][item["id"]] = true
      this.existenceCheck.push(item['id'])
    }
    this.dataService.saveShoppingBag(this.statuses[`${item["recipe_id"]}`], item["recipe_id"])
    console.log(this.statuses, this.existenceCheck)
  }
  strike(id) {
    this.strikes[id] = true
    this.dataService.saveStrikes(this.strikes)
  }

  unStrike(id) {
    this.strikes[id] = false
    this.dataService.saveStrikes(this.strikes)
  }
}
