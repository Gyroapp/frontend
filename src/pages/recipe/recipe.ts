import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ItemServiceProvider } from '../../providers/item-service/item-service';
import { Instruction } from '../../models/instructionModel';
import { Ingredient } from '../../models/ingredientModel';
import { RecipeTourPage } from '../recipe-tour/recipe-tour';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LoginPage } from '../login/login';
import { DataServiceProvider } from '../../providers/data-service/data-service'

@IonicPage()
@Component({
  selector: 'page-recipe',
  templateUrl: 'recipe.html',
  providers: [ItemServiceProvider, AuthServiceProvider, DataServiceProvider],
})

export class RecipePage implements OnInit {
  // Main recipe display page
  pocket = {}
  loggedIn: boolean
  recipe: any;
  instructions: Array<Instruction>
  ingredients: Array<Ingredient>
  user_id: number
  imageClicked: boolean = true
  data: any
  //ingThings: Array<any> = [];
  ingStatuses: any = {}; //= {};
  shoppingBagStatus: any = {};
  added = false
  constructor(public toast: ToastController, public navCtrl: NavController, public authService: AuthServiceProvider, public itemService: ItemServiceProvider, public navParams: NavParams, public dataService: DataServiceProvider) {
    this.recipe = this.navParams.data.data
  //  this.data = this.navParams.data.item
    //console.log('Recipe from RecipeHome', this.data)

    this.assignLoggedIn()
  }
  assignLoggedIn() {
    this.dataService.getToken().then((val) => {
      if (val != null) this.loggedIn = true;
    });
  }
  getUserData() {
    this.dataService.getData().then((userInfo) => {
      if (userInfo) {
        this.user_id = JSON.parse(userInfo).id;
        if(this.user_id) {
          this.dataService.checkLocallyIfAdded( this.recipe.id, this.user_id,).then(val => this.added = val)
        }
      }
    })
  }
  ngOnInit() {

    this.getUserData()
    this.assignIngredientStatuses()
  }
  assignIngredientStatuses() {
    this.dataService.getIngredientAndShoppingBagStatuses(this.recipe.id).then((val) => {
      if (val === null) {
        if (this.loggedIn === true) {
          this.dataService.getData().then((userInfo) => {
            this.user_id = JSON.parse(userInfo).id;
            this.dataService.getRecipeStatusFromApi(this.recipe.id, this.user_id).map(value => value.json()).subscribe(recipeStatus => {

              this.ingStatuses = recipeStatus.ingredient_statuses
              this.shoppingBagStatus = recipeStatus.shopping_bag_status
            }, error => {
              console.log(error)
            });
          });
        }
      }
      else {
        //   console.log(val)
        this.ingStatuses = val["ingredient_statuses"] || {}
        this.shoppingBagStatus = val["shopping_bag_status"] || {}
      }
      this.ingredientsAssign(this.recipe.item_id, this.recipe.id)
    })
  }
  ingredientsAssign(item_id, recipe_id) {
    this.dataService.getIngredientsFromStorage(recipe_id).then(ingredients => {
      this.ingredients = JSON.parse(ingredients)
      console.log(this.ingredients)
      if (this.ingredients != {})
        this.initStatuses()
      if (!this.ingredients) {
        this.itemService.getIngredients(item_id, recipe_id).subscribe((parsed_data) => {
          this.ingredients = parsed_data
          this.initStatuses()
          this.dataService.persistIngredientsByRecipeId(this.ingredients, recipe_id)
        })
      }
    })
  }
  initStatuses() {
    if (this.ingStatuses === null) {
      //this.ingStatuses = {};
      for (let i = 0; i < this.ingredients.length; i++) {
        this.ingStatuses[`${this.ingredients[i].id}`] = false
      }
    }
    if (this.loggedIn && this.shoppingBagStatus === null) {
      for (let i = 0; i < this.ingredients.length; i++) {
        this.shoppingBagStatus[`${this.ingredients[i].id}`] = false
      }
    }
  }
  recipeTour() {
    this.navCtrl.push(RecipeTourPage, { item_id: this.recipe.item_id, recipe: this.recipe, user_id: this.user_id });
  }
  backToLogin() {
    this.navCtrl.push(LoginPage);
  }

  addToShoppingBag(ingredient) {
    this.shoppingBagStatus[`${ingredient.id}`] = true
    this.dataService.saveShoppingBag(this.shoppingBagStatus, this.recipe.id)
    this.newToast("added to", ingredient)
  }
  removeFromShoppingBag(ingredient) {
    this.shoppingBagStatus[`${ingredient.id}`] = false
    this.dataService.removeFromShoppingBag(this.shoppingBagStatus, this.recipe.id, ingredient, this.user_id)
    this.newToast("removed from", ingredient)
  }
  statusChange(event, ing) {
    this.ingStatuses[`${ing.id}`] = event.value;
    this.dataService.saveIngStatuses(this.ingStatuses, this.recipe.id);
  }
  newToast(type, ingredient) {
    let toast = this.toast.create({
      message: `Ingredient ${ingredient.content} has been ${type} your shopping bag`,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }
  imageClick() {
    this.imageClicked = true;
  }

  addToPocketToast() {
    let toast = this.toast.create({
      message: "This recipe has been added to your pocket",
      duration: 1500,
      position: 'bottom'
    })
    toast.present()
  }

  removeFromPocketToast() {
    let toast = this.toast.create({
      message: "This recipe has been removed from your pocket",
      duration: 1500,
      position: 'bottom'
    })
    toast.present()
  }
  addToPocket(recipe_id) {
    this.dataService.addToPocket( recipe_id, this.user_id).subscribe(pocket => {
      console.log(pocket)
      this.added = true
      this.addToPocketToast()
      this.pocket = pocket
      this.dataService.updateLocalPocket(pocket)
    })
  }
  removeFromPocket(recipe_id) {
    this.dataService.removeFromPocket(recipe_id, this.user_id).subscribe(pocket => {
      this.added = false
      this.removeFromPocketToast()
      this.pocket = pocket
      this.dataService.updateLocalPocket(pocket)
    })
  }
}

