import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { ItemServiceProvider } from '../../providers/item-service/item-service';
import { TabsPage } from '../../pages/tabs/tabs'
@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit{
  special_items : any ;
  constructor(public navCtrl: NavController, public navParams: NavParams, public itemService : ItemServiceProvider) {
     }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
  ngOnInit(){
    this.itemService.getSpecialItems().subscribe((data) => {
      this.special_items = data;
      console.log(this.special_items);
      console.log(this.special_items[0].image_urls[0])
    });
 
  }
  openLogin(){
    this.navCtrl.push(LoginPage);
  }
  itemClick(item) {
    console.log("Item clicked:",item)
    this.navCtrl.push(TabsPage, { data: item })
    //this.nav.push(RecipePage,{data: item})

  }

}
