import { Component } from '@angular/core';
import { IonicPage, ViewController, NavController, NavParams } from 'ionic-angular';
import { DataServiceProvider } from '../../providers/data-service/data-service'

/**
 * Generated class for the RatingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-rating',
  templateUrl: 'rating.html',
})
export class RatingPage {
rate = 2.5;
review:any
recipe_id : number;
  constructor(private viewContoller:ViewController , public dataService: DataServiceProvider, public navCtrl: NavController, public navParams: NavParams) {
  this.review = this.navParams.get('review')
  this.recipe_id = this.navParams.get('recipe_id')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatingPage');
  }
postReview() {
console.log(this.recipe_id, this.review, this.rate)
    this.dataService.postReview(this.recipe_id, this.review, this.rate).then(
      (res) => {
        console.log(res);
      },
      (err)=>{
        console.log(err)
      }
    ).catch((err)=>{
        console.log(err)
      })
    this.navCtrl.pop()
  }
}
