export class Instruction {
    id : number;
    content : string;
    serial_number : number;
    recipe_id : number;
    created_at : string;
    updated_at : string;
    image_url : string;
}