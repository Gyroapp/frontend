import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app.module';
import { Nav } from 'ionic-angular';
platformBrowserDynamic().bootstrapModule(AppModule, [Nav]);
