import { BrowserModule } from '@angular/platform-browser'
import { ErrorHandler, NgModule } from '@angular/core'
import { IonicApp, IonicErrorHandler, IonicModule, Nav } from 'ionic-angular'
import { SplashScreen } from '@ionic-native/splash-screen'
import { StatusBar } from '@ionic-native/status-bar'
import { HttpModule } from '@angular/http'
import { MyApp } from './app.component'
import { RecipeHomePage } from '../pages/recipe-home/recipe-home'
import { ItemServiceProvider } from '../providers/item-service/item-service'
import { ItemsPage } from '../pages/items/items'
import { RecipePage } from '../pages/recipe/recipe'
import { AuthServiceProvider } from '../providers/auth-service/auth-service'
import { LoginPage } from '../pages/login/login'
import { RecipeTourPage } from '../pages/recipe-tour/recipe-tour'
import { IonicStorageModule } from '@ionic/storage'
import { AccountPage } from '../pages/account/account'
import { SignupPage } from '../pages/signup/signup'
import { DataServiceProvider } from '../providers/data-service/data-service'
import { NutritionPage } from '../pages/nutrition/nutrition'
import { TabsPage } from '../pages/tabs/tabs'
import { RatingPage } from '../pages/rating/rating'
import { HomePage } from '../pages/home/home'
import { CartPage } from '../pages/cart/cart'
import { ShoppingBagPage } from '../pages/shopping-bag/shopping-bag'
import { Ionic2RatingModule } from 'ionic2-rating'
import { ValidationServiceProvider } from '../providers/validation-service/validation-service'
import {CloudSettings, CloudModule} from '@ionic/cloud-angular'
import { PocketPage } from '../pages/pocket/pocket'
//import { InAppBrowser } from '@ionic-native/in-app-browser'
const cloud: CloudSettings = {
  'core':{
    'app_id': '29ef3c6e'
  },
  'auth': {
   'facebook': {
     'scope': ['public_profile', 'email']
   } 
  }
}

@NgModule({
  declarations: [
    MyApp,
    RecipeHomePage,
    ItemsPage,
    RecipePage,
    LoginPage,
    RecipeTourPage,
    AccountPage,
    SignupPage,
    NutritionPage,
    TabsPage,
    RatingPage,
    HomePage,
    CartPage,
    ShoppingBagPage,
    PocketPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    CloudModule.forRoot(cloud),
    IonicStorageModule.forRoot(),
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    RecipeHomePage,
    ItemsPage,
    RecipePage,
    LoginPage,
    RecipeTourPage,
    AccountPage,
    SignupPage,
    NutritionPage,
    TabsPage,
    RatingPage,
    HomePage,
    CartPage,
    ShoppingBagPage,
    PocketPage
  ],
  providers: [
    StatusBar,
    Nav,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ItemServiceProvider,
    AuthServiceProvider,
    Storage,
    DataServiceProvider,
    ValidationServiceProvider//,
  //  InAppBrowser
  ]

})
export class AppModule { }
