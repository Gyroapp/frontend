import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ItemServiceProvider } from '../providers/item-service/item-service'
import { HomePage } from '../pages/home/home';
import { ItemsPage } from '../pages/items/items';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { LoginPage } from '../pages/login/login';
import { AccountPage } from '../pages/account/account';
import { MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ShoppingBagPage } from '../pages/shopping-bag/shopping-bag';
import { DataServiceProvider } from '../providers/data-service/data-service';
import { PocketPage } from '../pages/pocket/pocket'
@Component({
  templateUrl: 'app.html',
  providers: [ItemServiceProvider, AuthServiceProvider]
})
export class MyApp {
  unauthPages: Array<{ title: string, component: any }>;
  authPages: Array<{ title: string, component: any }>;
  rootPage: any
  @ViewChild(Nav) nav: Nav;
  constructor(public dataService: DataServiceProvider, public storage: Storage, public menuController: MenuController, private authService: AuthServiceProvider, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public itemService: ItemServiceProvider) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.setDefaultParams()
      this.setMenu()
    });
  }
  public openPage(page) {
    this.nav.push(page.component);
  }

  setMenu() {
    this.dataService.getToken().then((val) => {
      if (val != null) {
        this.enableAuthMenu();
      }
      else this.enableUnauthMenu();
    }).catch(() => { this.enableUnauthMenu() });
  }
  setDefaultParams() {
    this.rootPage = PocketPage
    this.nav.setRoot(this.rootPage);
    this.unauthPages = [
      { title: 'Home', component: HomePage },
      { title: 'Items', component: ItemsPage },
      { title: 'Login', component: LoginPage },
    ]
    this.authPages = [
      { title: 'Home', component: HomePage },
      { title: 'Items', component: ItemsPage },
      {title: 'Pocket', component: PocketPage},
      { title: 'Shopping Bag', component: ShoppingBagPage},
    //  { title: 'Cart', component: CartPage },
      { title: 'Account', component: AccountPage },

    ]
  }
  enableUnauthMenu() {
    this.menuController.enable(true, 'unauthenticated');
    this.menuController.enable(false, 'authenticated');
  }
  enableAuthMenu() {
    this.menuController.enable(false, 'unauthenticated');
    this.menuController.enable(true, 'authenticated');
  }
}

